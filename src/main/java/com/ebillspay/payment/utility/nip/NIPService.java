package com.ebillspay.payment.utility.nip;

import com.ebillspay.payment.lib.entities.Transaction;
import com.ebillspay.payment.lib.util.ResponseCode;
import com.nibss.nip.NipException;
import com.nibss.nip.NipService;
import com.nibss.nip.crypto.FileSSMCipher;
import com.nibss.nip.dto.*;
import com.nibss.nip.impl.JaxwsNipService;
import com.nibss.nip.util.RandomGenerator;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NIPService {

    private String nibssCode;
    private String nipUrl;
    private int channelCode;
    private String keyDir;
    private String password;

    public NIPService() {

    }

    public NIPService(String nipUrl, String nibssCode) {
        this.nipUrl = nipUrl;
        this.nibssCode = nibssCode;
    }

    public NIPService(String nibssCode, int channelCode) {
        this.nibssCode = nibssCode;
        this.channelCode = channelCode;
    }

    public NIPService(String nipUrl, String nibssCode, int channelCode) {
        this.nipUrl = nipUrl;
        this.nibssCode = nibssCode;
        this.channelCode = channelCode;
    }

    public NIPService(String nipUrl, String nibssCode, int channelCode, String keyDir, String password) {
        this.nipUrl = nipUrl;
        this.nibssCode = nibssCode;
        this.channelCode = channelCode;
        this.keyDir = keyDir;
        this.password = password;
    }

    protected NESingleRequest convertToNameEnquiry(String accountNumber, String institutionCode) {
        NESingleRequest request = new NESingleRequest();
        if (accountNumber != null || !"".equals(accountNumber)) {
            request.setAccountNumber(accountNumber);
            request.setDestinationInstitutionCode(institutionCode);
            request.setChannelCode(channelCode);
            request.setSessionID(nibssCode + generateSessionID());
        }
        return request;
    }

    protected MandateAdviceRequest convertToMandateAdvice(Transaction debitAcct) {
        MandateAdviceRequest request = new MandateAdviceRequest();
        if (debitAcct != null) {
            request.setBeneficiaryAccountName(debitAcct.getBeneficiaryAccountName());
            request.setBeneficiaryAccountNumber(debitAcct.getBeneficiaryAccountNumber());
            request.setBeneficiaryBankVerificationNumber(toValue(debitAcct.getBeneficiaryBvn()));
            request.setBeneficiaryKYCLevel(debitAcct.getBeneficiaryKyc());
            request.setDestinationInstitutionCode(debitAcct.getSrcBankCode());
            request.setDebitAccountName(debitAcct.getSrcAccountName());
            request.setDebitAccountNumber(debitAcct.getSrcAccountNumber());
            request.setDebitBankVerificationNumber(toValue(debitAcct.getSrcBvn()));
            request.setDebitKYCLevel(debitAcct.getSrcKyc());
            request.setMandateReferenceNumber(debitAcct.getMandateRef() == null ? debitAcct.getSessionID() : debitAcct.getMandateRef());
            request.setAmount(debitAcct.getDebitAmount());
            request.setChannelCode(channelCode);
//            request.setSessionID(nibssCode + generateSessionID());
            request.setSessionID(debitAcct.getSessionID());
        }
        return request;
    }

    protected FTSingleDebitRequest convertToFTSingleDebit(Transaction deb) {
        FTSingleDebitRequest request = new FTSingleDebitRequest();
        if (deb != null) {
            request.setBeneficiaryAccountName(deb.getBeneficiaryAccountName());
            request.setBeneficiaryAccountNumber(deb.getBeneficiaryAccountNumber());
            request.setBeneficiaryBankVerificationNumber(toValue(deb.getBeneficiaryBvn()));
            request.setBeneficiaryKYCLevel(deb.getBeneficiaryKyc());
            request.setDestinationInstitutionCode(deb.getSrcBankCode());
            request.setNameEnquiryRef("");
            request.setDebitAccountName(deb.getSrcAccountName());
            request.setDebitAccountNumber(deb.getSrcAccountNumber());
            request.setDebitBankVerificationNumber(toValue(deb.getSrcBvn()));
            request.setDebitKYCLevel(deb.getSrcKyc());
            request.setMandateReferenceNumber(deb.getMandateRef());
            request.setTransactionFee(BigDecimal.ZERO);
            request.setAmount(deb.getDebitAmount());
            request.setChannelCode(channelCode);
            request.setNarration(toValue(deb.getNarration()));
            request.setPaymentReference(deb.getPaymentRef());
            request.setSessionID(deb.getSessionID());
            //request.setSessionID(nibssCode + deb.getSessionID().substring(6));
        }
        return request;
    }

    protected FTSingleCreditRequest convertToFTSingleCredit(Transaction creditTransaction) {
        FTSingleCreditRequest creditRequest = new FTSingleCreditRequest();
        if (creditTransaction != null) {
            creditRequest.setBeneficiaryAccountName(creditTransaction.getBeneficiaryAccountName());
            creditRequest.setBeneficiaryAccountNumber(creditTransaction.getBeneficiaryAccountNumber());
            creditRequest.setBeneficiaryKYCLevel(toValue(creditTransaction.getBeneficiaryKyc()));
            creditRequest.setBeneficiaryBankVerificationNumber(toValue(creditTransaction.getBeneficiaryBvn()));
            creditRequest.setDestinationInstitutionCode(creditTransaction.getBeneficiaryBankCode());
            creditRequest.setOriginatorAccountName(creditTransaction.getSrcAccountName());
            creditRequest.setOriginatorAccountNumber(creditTransaction.getSrcAccountNumber());
            creditRequest.setOriginatorKYCLevel(toValue(creditTransaction.getSrcKyc()));
            creditRequest.setOriginatorBankVerificationNumber(toValue(creditTransaction.getSrcBvn()));
            creditRequest.setNarration(toValue(creditTransaction.getNarration()));
            creditRequest.setPaymentReference(creditTransaction.getPaymentRef());
            creditRequest.setAmount(creditTransaction.getCreditAmount());
            creditRequest.setChannelCode(channelCode);
            creditRequest.setSessionID(creditTransaction.getSessionID());
        }
        return creditRequest;
    }

    protected FTSingleCreditRequest convertToFTSingleCredit(Transaction creditTransaction, boolean isFee) {
        FTSingleCreditRequest creditRequest = new FTSingleCreditRequest();
        if (creditTransaction != null) {
            creditRequest.setBeneficiaryAccountName(isFee ? creditTransaction.getFeeBeneficiaryAccountName() : creditTransaction.getBeneficiaryAccountName());
            creditRequest.setBeneficiaryAccountNumber(isFee ? creditTransaction.getFeeBeneficiaryAccountNumber() : creditTransaction.getBeneficiaryAccountNumber());
            creditRequest.setBeneficiaryKYCLevel(toValue(isFee ? creditTransaction.getFeeBeneficiaryKyc() : creditTransaction.getBeneficiaryKyc()));
            creditRequest.setBeneficiaryBankVerificationNumber(isFee ? toValue(creditTransaction.getFeeBeneficiaryBvn()) : toValue(creditTransaction.getBeneficiaryBvn()));
            creditRequest.setDestinationInstitutionCode(isFee ? creditTransaction.getFeeBeneficiaryBankCode() : creditTransaction.getBeneficiaryBankCode());
            creditRequest.setOriginatorAccountName(creditTransaction.getSrcAccountName());
            creditRequest.setOriginatorAccountNumber(creditTransaction.getSrcAccountNumber());
            creditRequest.setOriginatorKYCLevel(toValue(creditTransaction.getSrcKyc()));
            creditRequest.setOriginatorBankVerificationNumber(toValue(creditTransaction.getSrcBvn()));
            creditRequest.setNarration(toValue(creditTransaction.getNarration()));
            creditRequest.setPaymentReference(creditTransaction.getPaymentRef());
            creditRequest.setAmount(isFee ? creditTransaction.getFeeAmount() : creditTransaction.getCreditAmount());
            creditRequest.setChannelCode(channelCode);
            creditRequest.setSessionID(creditTransaction.getSessionID());
        }
        return creditRequest;
    }

    //NIP processes
    public NESingleResponse doNameEnquiry(String accountNumber, String institutionCode) throws NipException {
        try {
            if (nipUrl == null) {
                return null;
            }
            NESingleResponse res = createNipService().doNameEnquiry(convertToNameEnquiry(accountNumber, institutionCode));
            return res;
        } catch (MalformedURLException ex) {
            throw new NipException(ex);
        }
    }

    public MandateAdviceResponse doMandateAdvice(Transaction debit) throws NipException {
        try {
            if (nipUrl == null) {
                return null;
            }
            MandateAdviceResponse res = createNipService(debit.getSessionID().substring(0, 6)).doMandateAdvice(convertToMandateAdvice(debit));
            return res;
        } catch (MalformedURLException ex) {
            throw new NipException(ex);
        }
    }

    public String doNipDebit(Transaction debitTrans) {
        if (nipUrl == null || debitTrans == null) {
            return null;
        }
        try {
            boolean doMandateAdvice = debitTrans.getMandateRef() == null || debitTrans.getMandateRef().isEmpty();
            MandateAdviceResponse response = doMandateAdvice ? doMandateAdvice(debitTrans) : null;
            String responseCode = doMandateAdvice ? (response != null ? response.getResponseCode() : ResponseCode.SYSTEM_MALFUNCTION) : ResponseCode.SUCCESSFUL;
            if (!ResponseCode.SUCCESSFUL.equals(responseCode)) {
                return responseCode;
            }
            if (response != null && ResponseCode.SUCCESSFUL.equals(responseCode)) {
                debitTrans.setMandateRef(response.getMandateReferenceNumber());
            }
            FTSingleDebitResponse debitResponse = createNipService(debitTrans.getSessionID().substring(0, 6)).doFTSingleDebit(convertToFTSingleDebit(debitTrans));
            return debitResponse != null ? debitResponse.getResponseCode() : ResponseCode.REQUEST_IN_PROGRESS;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ResponseCode.REQUEST_IN_PROGRESS;
    }

    public String doNipCredit(Transaction creditTrans, boolean isFee) {
        if (nipUrl == null) {
            return null;
        }
        try {
            FTSingleCreditResponse response = createNipService(creditTrans.getSessionID().substring(0, 6)).doFTSingleCredit(convertToFTSingleCredit(creditTrans, isFee));
            return response != null ? response.getResponseCode() : ResponseCode.REQUEST_IN_PROGRESS;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ResponseCode.REQUEST_IN_PROGRESS;
    }


    /*
     * Utils
     */
    private NipService createNipService() throws MalformedURLException {
        return new JaxwsNipService(nipUrl, new FileSSMCipher(nibssCode, keyDir, password, nibssCode));
    }

    private NipService createNipService(String bankCode) throws MalformedURLException {
        return new JaxwsNipService(nipUrl, new FileSSMCipher(bankCode, keyDir, password, nibssCode));
    }

    private String generateSessionID() {
        return new SimpleDateFormat("yyMMddHHmmss").format(new Date()) + new RandomGenerator().getRandomLong(99999999999D, 999999999999D);
    }

    private Integer toInt(String value) {
        try {
            return (value != null) ? Integer.valueOf(value) : 0;
        } catch (Exception e) {

        }
        return 0;
    }

    private String toValue(String value) {
        return value == null ? "" : value;
    }

    private String toValue(int value) {
        return value == 0 ? "" : String.valueOf(value);
    }
}
